﻿namespace Schiffe_versenken
{
    partial class StartMenu
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(StartMenu));
            btnStartGame = new Button();
            imageList1 = new ImageList(components);
            label1 = new Label();
            label2 = new Label();
            textBox1 = new TextBox();
            pictureBox1 = new PictureBox();
            label3 = new Label();
            label4 = new Label();
            button1 = new Button();
            Normalbtn = new Button();
            Schwer = new Button();
            Statistiken = new Button();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            SuspendLayout();
            // 
            // btnStartGame
            // 
            btnStartGame.Location = new Point(53, 399);
            btnStartGame.Name = "btnStartGame";
            btnStartGame.Size = new Size(83, 23);
            btnStartGame.TabIndex = 0;
            btnStartGame.Text = "Spiel starten";
            btnStartGame.UseVisualStyleBackColor = true;
            btnStartGame.Click += btnStartGame_Click;
            // 
            // imageList1
            // 
            imageList1.ColorDepth = ColorDepth.Depth8Bit;
            imageList1.ImageStream = (ImageListStreamer)resources.GetObject("imageList1.ImageStream");
            imageList1.TransparentColor = Color.Transparent;
            imageList1.Images.SetKeyName(0, "IMG_6591.JPG");
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 13F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(53, 54);
            label1.Name = "label1";
            label1.Size = new Size(163, 25);
            label1.TabIndex = 2;
            label1.Text = "Schiffe versenken";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(53, 301);
            label2.Name = "label2";
            label2.Size = new Size(101, 15);
            label2.TabIndex = 3;
            label2.Text = "Gegner-IP-Adrese";
            // 
            // textBox1
            // 
            textBox1.Location = new Point(53, 319);
            textBox1.Name = "textBox1";
            textBox1.Size = new Size(100, 23);
            textBox1.TabIndex = 4;
            // 
            // pictureBox1
            // 
            pictureBox1.Image = (Image)resources.GetObject("pictureBox1.Image");
            pictureBox1.Location = new Point(274, 99);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new Size(502, 278);
            pictureBox1.TabIndex = 5;
            pictureBox1.TabStop = false;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new Font("Segoe UI", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label3.Location = new Point(37, 257);
            label3.Name = "label3";
            label3.Size = new Size(89, 19);
            label3.TabIndex = 6;
            label3.Text = "Mehrspieler";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new Font("Segoe UI", 10F, FontStyle.Bold, GraphicsUnit.Point);
            label4.Location = new Point(36, 113);
            label4.Name = "label4";
            label4.Size = new Size(92, 19);
            label4.TabIndex = 7;
            label4.Text = "Einzelspieler";
            // 
            // button1
            // 
            button1.Location = new Point(53, 135);
            button1.Name = "button1";
            button1.Size = new Size(100, 23);
            button1.TabIndex = 8;
            button1.Text = "Einfach";
            button1.UseVisualStyleBackColor = true;
            button1.Click += easybtn_Click;
            // 
            // Normalbtn
            // 
            Normalbtn.Location = new Point(54, 174);
            Normalbtn.Name = "Normalbtn";
            Normalbtn.Size = new Size(100, 23);
            Normalbtn.TabIndex = 9;
            Normalbtn.Text = "Normal";
            Normalbtn.UseVisualStyleBackColor = true;
            Normalbtn.Click += Normalbtn_Click;
            // 
            // Schwer
            // 
            Schwer.Location = new Point(53, 214);
            Schwer.Name = "Schwer";
            Schwer.Size = new Size(99, 23);
            Schwer.TabIndex = 10;
            Schwer.Text = "Schwer";
            Schwer.UseVisualStyleBackColor = true;
            Schwer.Click += hardbtn_Click;
            // 
            // Statistiken
            // 
            Statistiken.Location = new Point(675, 70);
            Statistiken.Name = "Statistiken";
            Statistiken.Size = new Size(75, 23);
            Statistiken.TabIndex = 11;
            Statistiken.Text = "Statistiken";
            Statistiken.UseVisualStyleBackColor = true;
            Statistiken.Click += Statistiken_Click;
            // 
            // StartMenu
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(Statistiken);
            Controls.Add(Schwer);
            Controls.Add(Normalbtn);
            Controls.Add(button1);
            Controls.Add(label4);
            Controls.Add(label3);
            Controls.Add(pictureBox1);
            Controls.Add(textBox1);
            Controls.Add(label2);
            Controls.Add(label1);
            Controls.Add(btnStartGame);
            Name = "StartMenu";
            Text = "Startmenue";
            FormClosed += StartMenu_FormClosed;
            Load += StartMenu_Load;
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button btnStartGame;
        private ImageList imageList1;
        private Label label1;
        private Label label2;
        private TextBox textBox1;
        private PictureBox pictureBox1;
        private Label label3;
        private Label label4;
        private Button button1;
        private Button Normalbtn;
        private Button Schwer;
        private Button Statistiken;
    }
}