﻿namespace Schiffe_versenken
{
    partial class Statistiken
    {
        private System.ComponentModel.IContainer components = null;
        private Label lblWinsEasy;
        private Label lblWinsNormal;
        private Label lblWinsHard;
        private Label lblWinsMultiplayer;
        private Label lblLossesEasy;
        private Label lblLossesNormal;
        private Label lblLossesHard;
        private Label lblLossesMultiplayer;

        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.lblWinsEasy = new System.Windows.Forms.Label();
            this.lblWinsNormal = new System.Windows.Forms.Label();
            this.lblWinsHard = new System.Windows.Forms.Label();
            this.lblWinsMultiplayer = new System.Windows.Forms.Label();
            this.lblLossesEasy = new System.Windows.Forms.Label();
            this.lblLossesNormal = new System.Windows.Forms.Label();
            this.lblLossesHard = new System.Windows.Forms.Label();
            this.lblLossesMultiplayer = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblWinsEasy
            // 
            this.lblWinsEasy.AutoSize = true;
            this.lblWinsEasy.Location = new System.Drawing.Point(13, 13);
            this.lblWinsEasy.Name = "lblWinsEasy";
            this.lblWinsEasy.Size = new System.Drawing.Size(75, 13);
            this.lblWinsEasy.TabIndex = 0;
            this.lblWinsEasy.Text = "Wins (Easy): 0";
            // 
            // lblWinsNormal
            // 
            this.lblWinsNormal.AutoSize = true;
            this.lblWinsNormal.Location = new System.Drawing.Point(13, 33);
            this.lblWinsNormal.Name = "lblWinsNormal";
            this.lblWinsNormal.Size = new System.Drawing.Size(88, 13);
            this.lblWinsNormal.TabIndex = 1;
            this.lblWinsNormal.Text = "Wins (Normal): 0";
            // 
            // lblWinsHard
            // 
            this.lblWinsHard.AutoSize = true;
            this.lblWinsHard.Location = new System.Drawing.Point(13, 53);
            this.lblWinsHard.Name = "lblWinsHard";
            this.lblWinsHard.Size = new System.Drawing.Size(75, 13);
            this.lblWinsHard.TabIndex = 2;
            this.lblWinsHard.Text = "Wins (Hard): 0";
            // 
            // lblWinsMultiplayer
            // 
            this.lblWinsMultiplayer.AutoSize = true;
            this.lblWinsMultiplayer.Location = new System.Drawing.Point(13, 73);
            this.lblWinsMultiplayer.Name = "lblWinsMultiplayer";
            this.lblWinsMultiplayer.Size = new System.Drawing.Size(111, 13);
            this.lblWinsMultiplayer.TabIndex = 3;
            this.lblWinsMultiplayer.Text = "Wins (Multiplayer): 0";
            // 
            // lblLossesEasy
            // 
            this.lblLossesEasy.AutoSize = true;
            this.lblLossesEasy.Location = new System.Drawing.Point(13, 93);
            this.lblLossesEasy.Name = "lblLossesEasy";
            this.lblLossesEasy.Size = new System.Drawing.Size(83, 13);
            this.lblLossesEasy.TabIndex = 4;
            this.lblLossesEasy.Text = "Losses (Easy): 0";
            // 
            // lblLossesNormal
            // 
            this.lblLossesNormal.AutoSize = true;
            this.lblLossesNormal.Location = new System.Drawing.Point(13, 113);
            this.lblLossesNormal.Name = "lblLossesNormal";
            this.lblLossesNormal.Size = new System.Drawing.Size(96, 13);
            this.lblLossesNormal.TabIndex = 5;
            this.lblLossesNormal.Text = "Losses (Normal): 0";
            // 
            // lblLossesHard
            // 
            this.lblLossesHard.AutoSize = true;
            this.lblLossesHard.Location = new System.Drawing.Point(13, 133);
            this.lblLossesHard.Name = "lblLossesHard";
            this.lblLossesHard.Size = new System.Drawing.Size(83, 13);
            this.lblLossesHard.TabIndex = 6;
            this.lblLossesHard.Text = "Losses (Hard): 0";
            // 
            // lblLossesMultiplayer
            // 
            this.lblLossesMultiplayer.AutoSize = true;
            this.lblLossesMultiplayer.Location = new System.Drawing.Point(13, 153);
            this.lblLossesMultiplayer.Name = "lblLossesMultiplayer";
            this.lblLossesMultiplayer.Size = new System.Drawing.Size(119, 13);
            this.lblLossesMultiplayer.TabIndex = 7;
            this.lblLossesMultiplayer.Text = "Losses (Multiplayer): 0";
            // 
            // Statistiken
            // 
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.lblLossesMultiplayer);
            this.Controls.Add(this.lblLossesHard);
            this.Controls.Add(this.lblLossesNormal);
            this.Controls.Add(this.lblLossesEasy);
            this.Controls.Add(this.lblWinsMultiplayer);
            this.Controls.Add(this.lblWinsHard);
            this.Controls.Add(this.lblWinsNormal);
            this.Controls.Add(this.lblWinsEasy);
            this.Name = "Statistiken";
            this.Load += new System.EventHandler(this.Statistiken_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
