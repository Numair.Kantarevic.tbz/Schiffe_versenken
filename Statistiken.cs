﻿namespace Schiffe_versenken
{
    public partial class Statistiken : Form
    {
        // The GameStatistics object to store and manage statistics data
        private GameStatistics _gameStatistics;

        // Singleton instance of the Statistiken class
        private static Statistiken _instance;

        // Property to get the singleton instance of Statistiken
        public static Statistiken Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new Statistiken();
                }
                return _instance;
            }
        }

        // Constructor for the Statistiken form
        public Statistiken()
        {
            InitializeComponent();
            _instance = this; // Set the singleton instance to this form
            _gameStatistics = new GameStatistics(); // Initialize the GameStatistics object
            LoadStatistics(); // Load saved statistics from file
            UpdateStatisticsLabels(); // Update the form's labels with the current statistics
        }

        // Method to save statistics to a file
        public void SaveStatistics()
        {
            // Open the file to write the statistics
            using (StreamWriter writer = new StreamWriter("gamestatistics.txt"))
            {
                writer.WriteLine(_gameStatistics.WinsEasy);
                writer.WriteLine(_gameStatistics.WinsNormal);
                writer.WriteLine(_gameStatistics.WinsHard);
                writer.WriteLine(_gameStatistics.WinsMultiplayer);
                writer.WriteLine(_gameStatistics.LossesEasy);
                writer.WriteLine(_gameStatistics.LossesNormal);
                writer.WriteLine(_gameStatistics.LossesHard);
                writer.WriteLine(_gameStatistics.LossesMultiplayer);
            }
        }

        // Method to load statistics from a file
        public void LoadStatistics()
        {
            // Check if the file exists before trying to read it
            if (File.Exists("gamestatistics.txt"))
            {
                // Read all lines from the file
                string[] lines = File.ReadAllLines("gamestatistics.txt");

                // Parse each line into the corresponding statistic
                _gameStatistics.WinsEasy = int.Parse(lines[0]);
                _gameStatistics.WinsNormal = int.Parse(lines[1]);
                _gameStatistics.WinsHard = int.Parse(lines[2]);
                _gameStatistics.WinsMultiplayer = int.Parse(lines[3]);
                _gameStatistics.LossesEasy = int.Parse(lines[4]);
                _gameStatistics.LossesNormal = int.Parse(lines[5]);
                _gameStatistics.LossesHard = int.Parse(lines[6]);
                _gameStatistics.LossesMultiplayer = int.Parse(lines[7]);
            }
        }

        // Event handler for form load event
        private void Statistiken_Load(object sender, EventArgs e)
        {
            // Initialize your form here if needed
        }

        // Enum to represent different game difficulties
        public enum Difficulty
        {
            Easy,
            Normal,
            Hard,
            Multiplayer
        }

        // Method to increment the win count for a specific difficulty
        public void IncrementWin(Difficulty difficulty)
        {
            switch (difficulty)
            {
                case Difficulty.Easy:
                    _gameStatistics.WinsEasy++;
                    break;
                case Difficulty.Normal:
                    _gameStatistics.WinsNormal++;
                    break;
                case Difficulty.Hard:
                    _gameStatistics.WinsHard++;
                    break;
                case Difficulty.Multiplayer:
                    _gameStatistics.WinsMultiplayer++;
                    break;
            }
            UpdateStatisticsLabels(); // Update the labels after incrementing the win
            SaveStatistics(); // Save the updated statistics to the file
        }

        // Method to increment the loss count for a specific difficulty
        public void IncrementLoss(Difficulty difficulty)
        {
            switch (difficulty)
            {
                case Difficulty.Easy:
                    _gameStatistics.LossesEasy++;
                    break;
                case Difficulty.Normal:
                    _gameStatistics.LossesNormal++;
                    break;
                case Difficulty.Hard:
                    _gameStatistics.LossesHard++;
                    break;
                case Difficulty.Multiplayer:
                    _gameStatistics.LossesMultiplayer++;
                    break;
            }
            UpdateStatisticsLabels(); // Update the labels after incrementing the loss
            SaveStatistics(); // Save the updated statistics to the file
        }

        // Method to update the labels with the current statistics
        public void UpdateStatisticsLabels()
        {
            lblWinsEasy.Text = $"Wins (Easy): {_gameStatistics.WinsEasy}";
            lblWinsNormal.Text = $"Wins (Normal): {_gameStatistics.WinsNormal}";
            lblWinsHard.Text = $"Wins (Hard): {_gameStatistics.WinsHard}";
            lblWinsMultiplayer.Text = $"Wins (Multiplayer): {_gameStatistics.WinsMultiplayer}";

            lblLossesEasy.Text = $"Losses (Easy): {_gameStatistics.LossesEasy}";
            lblLossesNormal.Text = $"Losses (Normal): {_gameStatistics.LossesNormal}";
            lblLossesHard.Text = $"Losses (Hard): {_gameStatistics.LossesHard}";
            lblLossesMultiplayer.Text = $"Losses (Multiplayer): {_gameStatistics.LossesMultiplayer}";
        }
    }
}
