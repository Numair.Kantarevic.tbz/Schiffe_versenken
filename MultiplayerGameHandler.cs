using WindowChat;

namespace Schiffe_versenken
{
    internal class MultiplayerGameHandler
    {
        private GameForm gameForm;
        private bool isOfflineGame;
        private Bothandler bothandler;
        private Difficulty difficulty;
        private List<List<Point>> placedShips;
        private List<Point> clickedPositions;

        // Add necessary fields
        private const int gridSize = 10; // Size of the game grid
        private Button[,] buttons1; // Buttons for the first player's grid
        private Button[,] buttons2; // Buttons for the second player's grid
        private bool verticalMode; // Indicates if the ship is placed vertically
        private int currentShipLength; // Length of the ship currently being placed
        private List<Point> currentShip; // Coordinates of the current ship being placed

        private int shipsOfSize5;
        private int shipsOfSize4;
        private int shipsOfSize3;
        private int shipsOfSize2;

        private enum GameState
        {
            PlacingShips, // State when the player is placing ships
            WaitingForOpponent, // State when waiting for the opponent's move
            Guessing, // State when the player is guessing the opponent's ship locations
            GameOver // State when the game is over
        }

        private GameState currentState;

        public MultiplayerGameHandler(GameForm gameForm, bool isOfflineGame, Difficulty difficulty)
        {
            this.gameForm = gameForm;
            this.isOfflineGame = isOfflineGame;
            this.difficulty = difficulty;
            placedShips = new List<List<Point>>(); // List to store placed ships
            clickedPositions = new List<Point>(); // List to store positions that have been clicked
            currentShip = new List<Point>(); // Initialize list for the current ship's coordinates
            currentShipLength = 5; // Start with the ship of length 5
            shipsOfSize5 = 0;
            shipsOfSize4 = 0;
            shipsOfSize3 = 0;
            shipsOfSize2 = 0;
            currentState = GameState.PlacingShips; // Set initial state to placing ships
        }

        public void HandleGameOver(bool playerWon)
        {
            currentState = GameState.GameOver; // Set the game state to GameOver

            if (Statistiken.Instance == null)
            {
                MessageBox.Show("Statistiken instance is not initialized!");
                return; // Exit early to avoid null reference
            }

            if (playerWon)
            {
                MessageBox.Show("Game Over! Du gewinnst!");

                // Notify opponent if it's an online game
                if (!isOfflineGame)
                {
                    SocketHandler.GetInstance().SendMessage("GameOver");
                }

                // Increment win statistics
                Statistiken.Instance.IncrementWin((Statistiken.Difficulty)difficulty);
            }
            else
            {
                MessageBox.Show("Game Over! Du hast verloren!");

                // Notify opponent if it's an online game
                if (!isOfflineGame)
                {
                    SocketHandler.GetInstance().SendMessage("GameOver");
                }

                // Increment loss statistics
                Statistiken.Instance.IncrementLoss((Statistiken.Difficulty)difficulty);
                Statistiken.Instance.UpdateStatisticsLabels();
            }

            // Close the application
            Application.Exit();
        }

        public void LockPanel(Panel panel, bool locked)
        {
            foreach (Control control in panel.Controls)
            {
                if (control is Button button)
                {
                    if (locked)
                    {
                        button.Enabled = false;
                    }
                    else
                    {
                        button.Enabled = button.BackColor == SystemColors.Control; // Only enable buttons with default control color
                    }
                }
            }
        }

        public void RandomlyPlaceShips()
        {
            // Clear any previously placed ships
            gameForm.clearbtn(null, null);

            Random random = new Random();
            int[] shipLengths = { 5, 4, 4, 3, 3, 3, 2, 2, 2, 2 };

            foreach (int shipLength in shipLengths)
            {
                bool placed = false;
                while (!placed)
                {
                    int row = random.Next(gridSize);
                    int col = random.Next(gridSize);
                    bool vertical = random.Next(2) == 0;
                    if (gameForm.ValidShipPosition(row, col, shipLength, vertical))
                    {
                        gameForm.PlaceShip(row, col, shipLength, vertical);
                        placed = true; // Ship placed successfully
                    }
                }
            }
        }
    }
}
