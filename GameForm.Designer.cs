﻿namespace Schiffe_versenken
{
    partial class GameForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }



        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            panel1 = new Panel();
            label1 = new Label();
            checkBox1 = new CheckBox();
            label2 = new Label();
            button1 = new Button();
            panel2 = new Panel();
            clearGridButton = new Button();
            panel3 = new Panel();
            textChatBox = new TextBox();
            textToSend = new TextBox();
            randomGridButton = new Button();
            panel3.SuspendLayout();
            SuspendLayout();
            // 
            // panel1
            // 
            panel1.Location = new Point(3, 60);
            panel1.Name = "panel1";
            panel1.Size = new Size(334, 288);
            panel1.TabIndex = 0;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 11F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(27, 9);
            label1.Name = "label1";
            label1.Size = new Size(133, 20);
            label1.TabIndex = 1;
            label1.Text = "Schiffe Versenken";
            // 
            // checkBox1
            // 
            checkBox1.AutoSize = true;
            checkBox1.Location = new Point(12, 365);
            checkBox1.Name = "checkBox1";
            checkBox1.Size = new Size(64, 19);
            checkBox1.TabIndex = 2;
            checkBox1.Text = "Vertikal";
            checkBox1.UseVisualStyleBackColor = true;
            checkBox1.CheckedChanged += checkBox1_CheckedChanged;
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(3, 42);
            label2.Name = "label2";
            label2.Size = new Size(59, 15);
            label2.TabIndex = 3;
            label2.Text = "Dein Feld:";
            // 
            // button1
            // 
            button1.Location = new Point(232, 365);
            button1.Name = "button1";
            button1.Size = new Size(75, 23);
            button1.TabIndex = 4;
            button1.Text = "bestätigen";
            button1.UseVisualStyleBackColor = true;
            button1.Click += bestätigenbtn;
            // 
            // panel2
            // 
            panel2.Location = new Point(429, 60);
            panel2.Name = "panel2";
            panel2.Size = new Size(322, 288);
            panel2.TabIndex = 5;
            // 
            // clearGridButton
            // 
            clearGridButton.Location = new Point(248, 34);
            clearGridButton.Name = "clearGridButton";
            clearGridButton.Size = new Size(98, 23);
            clearGridButton.TabIndex = 6;
            clearGridButton.Text = "Schiffe löschen";
            clearGridButton.UseVisualStyleBackColor = true;
            clearGridButton.Click += clearbtn;
            // 
            // panel3
            // 
            panel3.Controls.Add(textChatBox);
            panel3.Controls.Add(textToSend);
            panel3.Location = new Point(22, 415);
            panel3.Name = "panel3";
            panel3.Size = new Size(766, 184);
            panel3.TabIndex = 7;
            // 
            // textChatBox
            // 
            textChatBox.Enabled = false;
            textChatBox.Location = new Point(3, 36);
            textChatBox.Multiline = true;
            textChatBox.Name = "textChatBox";
            textChatBox.Size = new Size(554, 145);
            textChatBox.TabIndex = 1;
            // 
            // textToSend
            // 
            textToSend.Location = new Point(5, 13);
            textToSend.Name = "textToSend";
            textToSend.Size = new Size(554, 23);
            textToSend.TabIndex = 0;
            textToSend.KeyDown += textToSend_KeyDown;
            // 
            // randomGridButton
            // 
            randomGridButton.Location = new Point(113, 34);
            randomGridButton.Name = "randomGridButton";
            randomGridButton.Size = new Size(129, 23);
            randomGridButton.TabIndex = 8;
            randomGridButton.Text = "zufällige Formation ";
            randomGridButton.UseVisualStyleBackColor = true;
            randomGridButton.Click += randomGridButton_Click;
            // 
            // GameForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 611);
            Controls.Add(randomGridButton);
            Controls.Add(panel3);
            Controls.Add(clearGridButton);
            Controls.Add(panel2);
            Controls.Add(button1);
            Controls.Add(label2);
            Controls.Add(checkBox1);
            Controls.Add(label1);
            Controls.Add(panel1);
            Name = "GameForm";
            Text = "Battleship";
            FormClosed += GameForm_FormClosed;
            Load += GameForm_Load;
            panel3.ResumeLayout(false);
            panel3.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Panel panel1;
        private Label label1;
        private CheckBox checkBox1;
        private Label label2;
        private Button button1;
        private Panel panel2;
        private Button clearGridButton;
        private Panel panel3;
        private TextBox textToSend;
        private TextBox textChatBox;
        private Button randomGridButton;
    }
}