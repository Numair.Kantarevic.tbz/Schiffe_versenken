namespace Schiffe_versenken
{
    public class Bothandler
    {
        private List<Point> botGuesses = new List<Point>();
        private List<Point> botHits = new List<Point>();
        private List<List<Point>> playerShips = new List<List<Point>>();
        public List<List<Point>> botShips;
        private Random random = new Random();

        public Bothandler()
        {
            GenerateBotShipLayout(10);
        }

        public void SetPlayerShips(List<List<Point>> ships)
        {
            playerShips = ships;
            GenerateBotShipLayout(10);
        }
        public List<Point> GetPlayerShipsPoints()
        {
            return playerShips.SelectMany(list => list).ToList();
        }

        public List<Point> GetBotShipsPoints()
        {
            return botShips.SelectMany(list => list).ToList();
        }

        public void GenerateBotShipLayout(int gridSize)
        {
            botShips = new List<List<Point>>();
            int[] shipLengths = { 5, 4, 4, 3, 3, 3, 2, 2, 2, 2 };

            foreach (int shipLength in shipLengths)
            {
                bool placed = false;

                while (!placed)
                {
                    int row = random.Next(gridSize);
                    int col = random.Next(gridSize);
                    bool vertical = random.Next(2) == 0;

                    if (ValidBotShipPosition(row, col, shipLength, vertical, gridSize))
                    {
                        PlaceBotShip(row, col, shipLength, vertical);
                        placed = true;
                    }
                }
            }
        }

        private bool ValidBotShipPosition(int row, int col, int shipLength, bool vertical, int gridSize)
        {
            // Check if the ship is within the grid bounds
            if (vertical)
            {
                if (row + shipLength > gridSize) return false;
            }
            else
            {
                if (col + shipLength > gridSize) return false;
            }

            // Check each point of the ship and surrounding area
            for (int i = -1; i <= shipLength; i++)
            {
                for (int j = -1; j <= 1; j++)
                {
                    int r = vertical ? row + i : row + j;
                    int c = vertical ? col + j : col + i;

                    // Only check points within the grid bounds
                    if (r >= 0 && r < gridSize && c >= 0 && c < gridSize)
                    {
                        if (GetBotShipsPoints().Contains(new Point(r, c))) return false;
                    }
                }
            }

            return true;
        }


        private void PlaceBotShip(int row, int col, int shipLength, bool vertical)
        {
            List<Point> ship = new List<Point>();
            for (int i = 0; i < shipLength; i++)
            {
                int r = vertical ? row + i : row;
                int c = vertical ? col : col + i;
                ship.Add(new Point(r, c));
            }
            botShips.Add(ship);
        }



        public Point GetNextGuess(Difficulty difficulty)
        {
            switch (difficulty)
            {
                case Difficulty.Medium:
                    return GetMediumGuess();
                case Difficulty.Hard:
                    return GetHardGuess();
                case Difficulty.Easy:
                default:
                    return GetEasyGuess();
            }
        }

        public bool CheckHit(Point guess)
        {
            return GetPlayerShipsPoints().Contains(guess);
        }

        private Point GetEasyGuess()
        {
            Point guess;
            do
            {
                guess = new Point(random.Next(10), random.Next(10));
            } while (botGuesses.Contains(guess));

            botGuesses.Add(guess);
            return guess;
        }

        private Point GetMediumGuess()
        {
            if (botHits.Count > 0)
            {
                Point lastHit = botHits.Last();
                List<Point> potentialGuesses = new List<Point>
                {
                    new Point(lastHit.X - 1, lastHit.Y),
                    new Point(lastHit.X + 1, lastHit.Y),
                    new Point(lastHit.X, lastHit.Y - 1),
                    new Point(lastHit.X, lastHit.Y + 1)
                };

                foreach (Point potentialGuess in potentialGuesses)
                {
                    if (IsValidGuess(potentialGuess))
                    {
                        botGuesses.Add(potentialGuess);
                        return potentialGuess;
                    }
                }
            }

            return GetEasyGuess();
        }

        private Point GetHardGuess()
        {
            // If there are hits, try to determine the ship's orientation and continue hitting along that direction
            if (botHits.Count > 0)
            {
                Point lastHit = botHits.Last();
                List<Point> potentialGuesses = new List<Point>();

                // Try to guess around the last hit
                if (botHits.Count > 1)
                {
                    // Determine direction based on last two hits
                    Point secondLastHit = botHits[botHits.Count - 2];
                    bool isHorizontal = lastHit.Y == secondLastHit.Y;
                    bool isVertical = lastHit.X == secondLastHit.X;

                    if (isHorizontal)
                    {
                        potentialGuesses.Add(new Point(lastHit.X - 1, lastHit.Y));
                        potentialGuesses.Add(new Point(lastHit.X + 1, lastHit.Y));
                    }
                    if (isVertical)
                    {
                        potentialGuesses.Add(new Point(lastHit.X, lastHit.Y - 1));
                        potentialGuesses.Add(new Point(lastHit.X, lastHit.Y + 1));
                    }
                }
                else
                {
                    // If it's the first hit, check all four directions
                    potentialGuesses.AddRange(new List<Point>
            {
                new Point(lastHit.X - 1, lastHit.Y),
                new Point(lastHit.X + 1, lastHit.Y),
                new Point(lastHit.X, lastHit.Y - 1),
                new Point(lastHit.X, lastHit.Y + 1)
            });
                }

                foreach (Point potentialGuess in potentialGuesses)
                {
                    if (IsValidGuess(potentialGuess))
                    {
                        botGuesses.Add(potentialGuess);
                        return potentialGuess;
                    }
                }
            }

            // If no ship has been hit or no valid guess found in the direction, use a strategic approach
            return StrategicGuess();
        }

        private Point StrategicGuess()
        {
            // Prioritize guesses using a checkerboard pattern or in areas with fewer hits
            for (int x = 0; x < 10; x++)
            {
                for (int y = (x % 2 == 0) ? 0 : 1; y < 10; y += 2)
                {
                    Point guess = new Point(x, y);
                    if (IsValidGuess(guess))
                    {
                        botGuesses.Add(guess);
                        return guess;
                    }
                }
            }

            // Fallback to random guess if necessary
            return GetEasyGuess();
        }

        private bool IsValidGuess(Point guess)
        {
            return guess.X >= 0 && guess.X < 10 && guess.Y >= 0 && guess.Y < 10 && !botGuesses.Contains(guess);
        }

        public void RecordHit(Point hit)
        {
            botHits.Add(hit);
        }
    }
}
