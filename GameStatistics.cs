﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Schiffe_versenken
{
    public class GameStatistics
    {
        public int WinsEasy { get; set; }
        public int WinsNormal { get; set; }
        public int WinsHard { get; set; }
        public int WinsMultiplayer { get; set; }

        public int LossesEasy { get; set; }
        public int LossesNormal { get; set; }
        public int LossesHard { get; set; }
        public int LossesMultiplayer { get; set; }
    }
}
