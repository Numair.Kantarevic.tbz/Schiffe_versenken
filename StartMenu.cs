using WindowChat;

namespace Schiffe_versenken
{

    public enum Difficulty
    {
        Easy,
        Medium,
        Hard,
        Multiplayer
    }
    public partial class StartMenu : Form
    {
        public delegate void NavigateToGameWindowDelegate(bool isOfflineGame, Difficulty difficulty);

        public StartMenu()
        {
            InitializeComponent();
        }

        private void IsConnected()
        {
            // Use BeginInvoke to ensure that navigation happens on the UI thread
            BeginInvoke(new NavigateToGameWindowDelegate(NavigateToGameWindow), false, Difficulty.Multiplayer);
        }

        private void NavigateToGameWindow(bool isOfflineGame, Difficulty difficulty)
        {
            GameForm gameWindow = new GameForm(isOfflineGame, difficulty);
            gameWindow.Show();
            this.Hide();
        }

        private void btnStartGame_Click(object sender, EventArgs e)
        {
            btnStartGame.Enabled = false;
            string ipAddress = textBox1.Text;
            bool success = SocketHandler.GetInstance().Connect(ipAddress);

            if (success)
            {
                NavigateToGameWindow(false, Difficulty.Multiplayer);
            }
            else
            {
                MessageBox.Show("Connection failed");
            }
            btnStartGame.Enabled = true;
        }

        private void StartMenu_Load(object sender, EventArgs e)
        {
            SocketHandler.GetInstance().connected += IsConnected;
        }

        private void StartMenu_FormClosed(object sender, FormClosedEventArgs e)
        {
            SocketHandler.GetInstance().connected -= IsConnected;
        }

        private void Normalbtn_Click(object sender, EventArgs e)
        {
            NavigateToGameWindow(true, Difficulty.Medium);
        }

        private void easybtn_Click(object sender, EventArgs e)
        {
            NavigateToGameWindow(true, Difficulty.Easy);
        }

        private void hardbtn_Click(object sender, EventArgs e)
        {
            NavigateToGameWindow(true, Difficulty.Hard);
        }

        private void Statistiken_Click(object sender, EventArgs e)
        {
            Statistiken statisticsForm = new Statistiken();
            statisticsForm.ShowDialog();
        }
    }
}
