﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Windows.Forms;

namespace WindowChat
{
    public sealed class SocketHandler
    {
        private static Socket _socket; // Communication Socket
        private static TcpListener _listener; // Listener Client for the Server
        private static SocketHandler instance; // the one and only instance of this class
        private static int _iPort = 5080; // Currently used port
        private static string _IpAddress; // IP Address of the chat partner
        private static string _ReveivedMsg;
        private byte[] _buffer = new byte[1028]; //buffer to receive the text

        // Delegated Events to use in Form
        public delegate void SystemConnected();
        public event SystemConnected connected;
        public delegate void TextReceived(string Text);
        public event TextReceived txtReveived;
        public delegate void ConnectionClosed();
        public event ConnectionClosed disconnected;

        // Private constructor for Singleton
        private SocketHandler()
        {
            try
            {
                _listener = new TcpListener(IPAddress.Any, _iPort);
                _listener.Start();
                AcceptClient();
            }
            catch (SocketException socketexception)
            {
                if (socketexception.ErrorCode == 10048)
                {
                    MessageBox.Show("There is already a server opened.", "Cant listen to connections.", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            catch (Exception ex)
            {
                String Except = ex.ToString();
            }
        }

        // Get the Instance from the one object that exists
        public static SocketHandler GetInstance()
        {
            if (instance == null)
            {
                instance = new SocketHandler();
            }
            return instance;
        }

        // Close the current connection
        public void CloseSocket()
        {
            try
            {
                if (_listener != null)
                {
                    _listener.Stop();
                }
                if (_socket != null)
                {
                    if (_socket.IsBound)
                    {
                        _socket.Close();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        public void Dispose()
        {
            if (_listener != null)
            {
                _listener.Stop();
            }
            if (_socket != null)
            {
                _socket.Close();
            }
        }

        // Connects to the server
        public bool Connect(string IpAddress)
        {
            bool bReturn = false;
            _IpAddress = IpAddress;
            try
            {
                _listener.Stop();
                _socket = new Socket(SocketType.Stream, ProtocolType.Tcp);
                _socket.Connect(_IpAddress, _iPort);
                _socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, ProcessMessageReceived, null);
                bReturn = true;
            }
            catch (Exception ex)
            {
                bReturn = false;
            }
            return bReturn;
        }

        // Accept the client
        private void AcceptClient()
        {
            _listener.BeginAcceptSocket(ProcessConnectionEstablished, null);
        }

        // Sends text to the chat partner
        public bool SendMessage(string message)
        {
            bool bReturn = false;
            if (_socket.Connected == false) 
            {
                return false;
            }
            else
            {
                try
                {
                    byte[] buffer = Encoding.ASCII.GetBytes(message);
                    _socket.Send(buffer);
                    bReturn = true;
                }
                catch (Exception ex)
                {
                    bReturn = false;
                }
            }
            return bReturn;
        }

        // Connection is established
        public void ProcessConnectionEstablished(IAsyncResult result)
        {
            try
            {
                _socket = _listener.EndAcceptSocket(result);
                _buffer = new byte[1028];
                // start the listener Socket
                _socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, ProcessMessageReceived, null);
                connected();
            }
            catch (Exception ex)
            {
            }
        }

        // Message from Chat Partner received
        void ProcessMessageReceived(IAsyncResult result)
        {
            try
            {
                if (result.IsCompleted)
                {
                    int bytesRead = _socket.EndReceive(result);
                    if (bytesRead > 0)
                    {
                        string tmp = Encoding.ASCII.GetString(_buffer, 0, bytesRead);
                        _ReveivedMsg = tmp;

                        if (txtReveived != null)
                            txtReveived(_ReveivedMsg);

                        // Clear the buffer for the next receive
                        Array.Clear(_buffer, 0, _buffer.Length);

                        // restart the listener Socket
                        _socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, ProcessMessageReceived, null);
                    }
                }
            }
            catch (Exception ex)
            {
                string Except = ex.ToString();
                if (disconnected != null)
                    disconnected();
            }
        }
    }
}
