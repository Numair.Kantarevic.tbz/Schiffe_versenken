using WindowChat;

namespace Schiffe_versenken
{
    public partial class GameForm : Form
    {
        private MultiplayerGameHandler gameHandler;
        private Difficulty difficulty;
        private Bothandler bothandlerInstance;
        private const int gridSize = 10;
        private Button[,] buttons1;
        private Button[,] buttons2;
        private List<Point> clickedPositions;
        private bool verticalMode;
        private int currentShipLength;
        private List<Point> currentShip;

        private int shipsOfSize5;
        private int shipsOfSize4;
        private int shipsOfSize3;
        private int shipsOfSize2;

        private List<List<Point>> placedShips;

        private enum GameState
        {
            PlacingShips,
            WaitingForOpponent,
            Guessing,
            GameOver
        }

        private GameState currentState;
        private bool opponentReady;
        private bool isOfflineGame;
        string ChatText;
        private bool isPlayerTurn;

        public GameForm(bool isOfflineGame, Difficulty difficulty)
        {
            InitializeComponent();
            this.isOfflineGame = isOfflineGame;
            this.difficulty = difficulty;
            clickedPositions = new List<Point>();
            currentShip = new List<Point>();
            placedShips = new List<List<Point>>();
            currentShipLength = 5; // Start with the ship of length 5
            shipsOfSize5 = 0;
            shipsOfSize4 = 0;
            shipsOfSize3 = 0;
            shipsOfSize2 = 0;
            bothandlerInstance = new Bothandler();
            currentState = GameState.PlacingShips;
            opponentReady = false;
            gameHandler = new MultiplayerGameHandler(this, isOfflineGame, difficulty);
            isPlayerTurn = true;
        }

        private void GameForm_Load(object sender, EventArgs e)
        {
            buttons1 = CreateGrid(panel1, 30, 5, 5);
            buttons2 = CreateGrid(panel2, 30, 5, 5);

            // Register for the TextReceived event of the SocketHandler class
            if (!isOfflineGame)
            {
                // Online mode setup
                SocketHandler.GetInstance().txtReveived += SocketHandler_TextReceived;
                SocketHandler.GetInstance().connected += OnConnected;
                SocketHandler.GetInstance().disconnected += OnDisconnected;
            }
            else
            {
                SocketHandler.GetInstance().CloseSocket();
                textToSend.Visible = false;
                panel3.Visible = false;
            }

        }

        private void SocketHandler_TextReceived(string text)
        {
            Console.WriteLine($"Received: {text}");
            if (isOfflineGame) return;

            if (InvokeRequired)
            {
                Invoke(new Action<string>(SocketHandler_TextReceived), text);
                return;
            }

            if (text == "Ready")
            {
                opponentReady = true;
                if (currentState == GameState.WaitingForOpponent)
                {
                    isPlayerTurn = false;
                    StartGuessingPhase();
                    gameHandler.LockPanel(panel2, !isPlayerTurn);
                }
            }
            else if (text.StartsWith("GuessResponse"))
            {
                isPlayerTurn = true;
                HandleGuessResponse(text);
            }
            else if (text.StartsWith("Guess"))
            {
                string[] parts = text.Split(' ');
                if (parts.Length == 3)
                {
                    int row = int.Parse(parts[1]);
                    int col = int.Parse(parts[2]);
                    bool hit = clickedPositions.Contains(new Point(row, col));
                    bool shipDestroyed = false;

                    if (hit)
                    {
                        List<Point> ship = placedShips.FirstOrDefault(s => s.Contains(new Point(row, col)));
                        if (ship != null)
                        {
                            // Mark the current position as hit
                            buttons1[row, col].BackColor = Color.Blue;

                            // Check if all parts of the ship are hit
                            shipDestroyed = ship.All(p => buttons1[p.X, p.Y].BackColor == Color.Blue);

                            if (shipDestroyed)
                            {
                                // Ship is destroyed, send a message with ship's positions
                                string shipPositions = string.Join(";", ship.Select(p => $"{p.X},{p.Y}"));
                                string destroyMessage = $"ShipDestroyed {shipPositions}";
                                SocketHandler.GetInstance().SendMessage(destroyMessage);
                            }
                        }
                    }

                    string response = $"GuessResponse {row} {col} {hit} {shipDestroyed}";
                    SocketHandler.GetInstance().SendMessage(response);

                    // Update opponent's guess on the player's grid
                    Button button = buttons1[row, col];
                    button.BackColor = hit ? Color.Blue : Color.Gray;
                    button.Enabled = false; // Disable the button after it's been guessed

                    if (!hit)
                    {
                        // Only toggle the player's turn if it was a miss
                        TogglePlayerTurn();
                    }
                }
            }
            else if (text.StartsWith("ShipDestroyed"))
            {
                string[] parts = text.Substring(13).Split(';'); // Remove "ShipDestroyed " prefix
                List<Point> ship = new List<Point>();

                foreach (string part in parts)
                {
                    string[] coords = part.Split(',');
                    int x = int.Parse(coords[0]);
                    int y = int.Parse(coords[1]);
                    ship.Add(new Point(x, y));
                }

                DisableCellsAroundShip(ship, buttons2);
            }

            else if (text == "GameOver")
            {
                gameHandler.HandleGameOver(false);
            }
            else if (text.StartsWith("chat"))
            {
                string chatMessage = text.Substring(5); // Remove "chat " prefix
                ChatText = "Friend: " + chatMessage + "\r\n" + ChatText;
                textChatBox.BeginInvoke(new MethodInvoker(() =>
                {
                    textChatBox.Text = ChatText;
                }));
            }
        }

        private void OnConnected()
        {
            MessageBox.Show("Verbindung wurde hergestellt.");
        }

        private void OnDisconnected()
        {
            MessageBox.Show("Verbindung verloren.");
        }

        private void StartGuessingPhase()
        {
            currentState = GameState.Guessing;
            gameHandler.LockPanel(panel2, true);
            TogglePlayerTurn();
        }

        private void TogglePlayerTurn()
        {
            if (InvokeRequired)
            {
                Invoke(new Action(TogglePlayerTurn));
                return;
            }
            isPlayerTurn = !isPlayerTurn;
            if (!isOfflineGame)
            {
                gameHandler.LockPanel(panel2, !isPlayerTurn); // Player's panel (panel2) should be unlocked during their turn
            }
            else
            {
                gameHandler.LockPanel(panel2, isPlayerTurn);
            }
        }

        public void HandleGuessResponse(string text)
        {
            if (InvokeRequired)
            {
                Invoke(new Action<string>(HandleGuessResponse), text);
                return;
            }

            string[] parts = text.Split(' ');
            if (parts.Length == 5 && parts[0] == "GuessResponse")
            {
                int row = int.Parse(parts[1]);
                int col = int.Parse(parts[2]);
                bool hit = bool.Parse(parts[3]);
                bool shipDestroyed = bool.Parse(parts[4]);

                Button button = buttons2[row, col]; // Use buttons2 for the opponent's grid
                button.BackColor = hit ? Color.Red : Color.Gray;
                button.Enabled = false;

                if (hit)
                {
                    if (CheckAllShipsSunk())
                    {
                        gameHandler.HandleGameOver(true);
                        return;
                    }
                }
                else
                {
                    TogglePlayerTurn();
                    if (isOfflineGame)
                    {
                        MakeBotMove();
                    }
                }
            }
        }

        private void MakeBotMove()
        {
            Point botGuess = bothandlerInstance.GetNextGuess(difficulty);

            // Add boundary check
            if (botGuess.X < 0 || botGuess.X >= gridSize || botGuess.Y < 0 || botGuess.Y >= gridSize)
            {
                Console.WriteLine($"Invalid bot guess: {botGuess}");
                return;
            }

            bool hit = bothandlerInstance.CheckHit(botGuess);

            // Update the UI to reflect the bot's guess
            Button button = buttons1[botGuess.X, botGuess.Y];
            button.BackColor = hit ? Color.Blue : Color.Gray;
            button.Enabled = false; // Disable the button after bot's guess

            if (hit)
            {
                bothandlerInstance.RecordHit(botGuess);
                if (CheckAllBotShipsSunk())
                {
                    Console.WriteLine("Alle Spieler-Schiffe wurden versenkt!");
                    gameHandler.HandleGameOver(false);
                    return;
                }
            }
        }

        private Button[,] CreateGrid(Panel panel, int size, int gap, int xOffset)
        {
            Button[,] buttons;
            panel.Controls.Clear();
            buttons = new Button[gridSize, gridSize];

            int panelWidth = gridSize * 40 + 2 * gap;
            panel.Size = new Size(panelWidth, gridSize * 40);

            for (int row = 0; row < gridSize; row++)
            {
                for (int col = 0; col < gridSize; col++)
                {
                    Button button = new Button();
                    button.Name = $"btn_{row}_{col}";
                    button.Text = "";
                    button.Size = new Size(size, size);
                    button.Click += GridButton_Click;
                    button.Location = new Point(row * size + xOffset, col * size + gap);
                    panel.Controls.Add(button);
                    buttons[row, col] = button;

                    // Disable the buttons in panel2 initially
                    if (panel == panel2)
                    {
                        button.Enabled = false;
                    }
                }
            }
            return buttons;
        }

        public bool CheckAllShipsSunk()
        {
            int redCount = 0;
            foreach (var button in buttons2)
            {
                if (button.BackColor == Color.Red)
                {
                    redCount++;
                }
            }
            return redCount == 30; // All ship cells should be red when all ships are sunk
        }

        private void GridButton_Click(object sender, EventArgs e)
        {
            Button clickedButton = sender as Button;
            if (clickedButton != null)
            {
                string buttonName = clickedButton.Name;
                string[] nameParts = buttonName.Split('_');
                int row = Int32.Parse(nameParts[1]);
                int col = Int32.Parse(nameParts[2]);

                Point clickedPoint = new Point(row, col);

                if (currentState == GameState.PlacingShips)
                {
                    int shipLength = GetNextShipLength();
                    bool isVertical = checkBox1.Checked;

                    if (shipLength > 0)
                    {
                        if (isVertical)
                        {
                            if (ValidShipPosition(row, col, shipLength, true))
                            {
                                PlaceShip(row, col, shipLength, true);
                            }
                            else
                            {
                                MessageBox.Show("Schiff kann nicht platziert werden. Bitte wählen Sie eine andere Position.");
                            }
                        }
                        else
                        {
                            if (ValidShipPosition(row, col, shipLength, false))
                            {
                                PlaceShip(row, col, shipLength, false);
                            }
                            else
                            {
                                MessageBox.Show("Schiff kann nicht platziert werden. Bitte wählen Sie eine andere Position.");
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Alle Schiffe wurden platziert.");
                    }
                }
                else if (currentState == GameState.Guessing)
                {
                    HandleGuess(clickedButton, clickedPoint);
                }
            }
        }

        public bool AreAllShipsPlaced()
        {
            int totalShipSegments = 5 * 1 + 4 * 2 + 3 * 3 + 2 * 4;

            int redCount = 0;
            foreach (var button in buttons1)
            {
                if (button.BackColor == Color.Red)
                {
                    redCount++;
                }
            }

            return redCount == totalShipSegments; // Check if all segments are placed
        }

        private int GetNextShipLength()
        {
            if (shipsOfSize5 < 1) return 5;
            else if (shipsOfSize4 < 2) return 4;
            else if (shipsOfSize3 < 3) return 3;
            else if (shipsOfSize2 < 4) return 2;
            else return 0;
        }

        public bool ValidShipPosition(int row, int col, int shipLength, bool vertical)
        {
            // Ensure the ship fits within the grid
            if (vertical)
            {
                if (row + shipLength > gridSize) return false;
            }
            else
            {
                if (col + shipLength > gridSize) return false;
            }

            // Check if the ship overlaps or is adjacent to any existing ships
            for (int i = 0; i < shipLength; i++)
            {
                int r = vertical ? row + i : row;
                int c = vertical ? col : col + i;

                if (buttons1[r, c].BackColor == Color.Red) return false;

                foreach (var pos in clickedPositions)
                {
                    if (Math.Abs(pos.X - r) <= 1 && Math.Abs(pos.Y - c) <= 1) return false;
                }
            }

            return true;
        }

        public void PlaceShip(int row, int col, int shipLength, bool isVertical)
        {
            List<Point> newShip = new List<Point>();

            for (int i = 0; i < shipLength; i++)
            {
                int r = isVertical ? row + i : row;
                int c = isVertical ? col : col + i;

                buttons1[r, c].BackColor = Color.Red;
                buttons1[r, c].Enabled = false;
                clickedPositions.Add(new Point(r, c));
                newShip.Add(new Point(r, c));
            }

            placedShips.Add(newShip);

            // Update the number of placed ships based on the length of the current ship
            if (shipLength == 5) shipsOfSize5++;
            else if (shipLength == 4) shipsOfSize4++;
            else if (shipLength == 3) shipsOfSize3++;
            else if (shipLength == 2) shipsOfSize2++;
        }

        private bool CheckAllBotShipsSunk()
        {
            int blueCount = 0;
            foreach (var button in buttons1)
            {
                if (button.BackColor == Color.Blue)
                {
                    blueCount++;
                }
            }
            return blueCount == 30; // All ship cells should be blue when all bot's ships are sunk
        }

        private void HandleGuess(Button clickedButton, Point clickedPoint)
        {
            if (clickedButton.Enabled == false)
            {
                return; // If the button is already disabled, do nothing
            }

            clickedButton.Enabled = false; // Disable the button after guessing
            clickedButton.BackColor = Color.Gray; // Default color for missed guess
            if (isOfflineGame)
            {
                bool hit = bothandlerInstance.GetBotShipsPoints().Contains(clickedPoint);
                clickedButton.BackColor = hit ? Color.Red : Color.Gray;
                if (hit)
                {
                    bothandlerInstance.RecordHit(clickedPoint); // Record the hit
                    // Check if the entire ship has been sunk
                    List<Point>? ship = bothandlerInstance.botShips.FirstOrDefault(s => s.Contains(clickedPoint));
                    if (ship != null && ship.All(p => buttons2[p.X, p.Y].BackColor == Color.Red))
                    {
                        // Disable cells around the sunk ship
                        DisableCellsAroundShip(ship, buttons2);
                    }
                    if (CheckAllShipsSunk())
                    {
                        gameHandler.HandleGameOver(true);
                        return;
                    }
                }
                else
                {
                    MakeBotMove();
                }
            }
            else
            {
                string guessMessage = $"Guess {clickedPoint.X} {clickedPoint.Y}";
                SocketHandler.GetInstance().SendMessage(guessMessage);
            }
        }

        public void DisableCellsAroundShip(List<Point> ship, Button[,] grid)
        {
            foreach (Point point in ship)
            {
                for (int row = point.X - 1; row <= point.X + 1; row++)
                {
                    for (int col = point.Y - 1; col <= point.Y + 1; col++)
                    {
                        if (row >= 0 && row < gridSize && col >= 0 && col < gridSize)
                        {
                            Button button = grid[row, col];

                            if (button.BackColor != Color.Red) // Don't disable the ship itself
                            {
                                button.Enabled = false;
                                button.BackColor = Color.Gray; // Set the background color to gray
                            }
                        }
                    }
                }
            }
        }


        private void bestätigenbtn(object sender, EventArgs e)
        {
            if (AreAllShipsPlaced())
            {
                (sender as Button).Enabled = false;
                if (!isOfflineGame)
                {
                    isPlayerTurn = true;
                    SocketHandler.GetInstance().SendMessage("Ready");
                    gameHandler.LockPanel(panel2, !isPlayerTurn);
                }

                randomGridButton.Enabled = false;
                clearGridButton.Enabled = false;
                gameHandler.LockPanel(panel1, true);
                currentState = GameState.WaitingForOpponent;

                if (isOfflineGame)
                {
                    bothandlerInstance.SetPlayerShips(placedShips);
                    opponentReady = true;
                    StartGuessingPhase();
                }
                else
                {
                    if (opponentReady)
                    {
                        StartGuessingPhase();
                    }
                }
            }
            else
            {
                MessageBox.Show("Bitte platzieren Sie alle Schiffe, bevor Sie die Aufstellung bestätigen.", "Fehlende Schiffe", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        public void clearbtn(object sender, EventArgs e)
        {
            gameHandler.LockPanel(panel1, false);
            // Clear all ships and reset the game state
            clickedPositions.Clear();
            currentShip.Clear();
            placedShips.Clear();
            shipsOfSize5 = 0;
            shipsOfSize4 = 0;
            shipsOfSize3 = 0;
            shipsOfSize2 = 0;
            currentShipLength = 5;

            // Reset all buttons to their default state
            for (int row = 0; row < gridSize; row++)
            {
                for (int col = 0; col < gridSize; col++)
                {
                    buttons1[row, col].BackColor = SystemColors.ControlLightLight;
                    buttons1[row, col].Enabled = true;
                    buttons1[row, col].Text = "";
                }
            }
        }

        private void randomGridButton_Click(object sender, EventArgs e)
        {
            gameHandler.RandomlyPlaceShips();
            gameHandler.LockPanel(panel1, true);
        }

        private void GameForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            SocketHandler.GetInstance().CloseSocket();
            SocketHandler.GetInstance().Dispose();
            if (!isOfflineGame)
            {
                SocketHandler.GetInstance().SendMessage("disconnect");
            }
        }

        private void textToSend_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string textSend;
                string messageToSend;
                if (!isOfflineGame)
                {
                    textSend = textToSend.Text;
                    messageToSend = "chat " + textSend;
                    if (SocketHandler.GetInstance().SendMessage(messageToSend))
                    {
                        ChatText = "You: " + textSend + "\r\n" + ChatText;
                        textToSend.Text = "";
                        textChatBox.Text = ChatText;
                    }
                }
                else
                {
                    textChatBox.Enabled = false;
                }
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            verticalMode = checkBox1.Checked;
        }
    }
}
